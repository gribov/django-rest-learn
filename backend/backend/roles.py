from rolepermissions.roles import AbstractUserRole

class Administrator(AbstractUserRole):
    available_permissions = {
        'manage_users': True,
        'add_jobs': True,
    }

class User(AbstractUserRole):
    available_permissions = {
        'add_jobs': True,
    }