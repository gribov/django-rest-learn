from django.contrib.auth.models import Group, User
from rest_framework import serializers
from backend.quickstart.models import Job


class UserSerializer(serializers.ModelSerializer):

    jobs = serializers.PrimaryKeyRelatedField(many=True, queryset=Job.objects.all())

    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups', 'is_superuser', 'jobs')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class JobListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Job
        fields = ('user', 'url', 'title', 'deadline')
    user = serializers.ReadOnlyField(source='user.id')

class JobDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Job
        fields = ('user', 'title', 'description', 'deadline')
    user = serializers.ReadOnlyField(source='user.id')