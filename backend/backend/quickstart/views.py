from django.contrib.auth.models import Group, User
from backend.quickstart.models import Job
from rest_framework import viewsets
from rest_framework import generics
from backend.quickstart.serializers import UserSerializer, GroupSerializer, JobListSerializer, JobDetailSerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly
from backend.quickstart.permissions import IsJobOwnerOrReadOnly

@permission_classes((IsAdminUser, ))
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

@permission_classes((IsAuthenticated, ))
class JobDetail(generics.RetrieveAPIView):
    queryset = Job.objects.all()
    serializer_class = JobDetailSerializer

@permission_classes((IsAdminUser, ))
class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

@permission_classes((IsAuthenticated, IsJobOwnerOrReadOnly))
class JobViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Job.objects.all()
    serializer_class = JobListSerializer

    def get_queryset(self):
        return Job.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def retrieve(self, request, *args, **kwargs):
        job = self.get_object()
        serializer = JobDetailSerializer(job, context={'request': request})
        return Response(serializer.data)

@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated, ))
def createUser(request):
    user = User.objects.get(username='genry')
    user.set_password('pass')
    user.is_staff = 0
    user.save()
    serializer = UserSerializer(user, context={'request': request})
    return Response(serializer.data)