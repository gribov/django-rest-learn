from django.db import models
from django.contrib.auth.models import User

class Job(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="jobs")
	title = models.CharField(max_length=100)
	description = models.CharField(max_length=500)
	deadline = models.DateField()